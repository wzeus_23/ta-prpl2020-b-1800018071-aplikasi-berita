<?php

require '../config/db_connect.php';
require '../config/function.libs.php';
session_start();

?>

<html>
    <head>
        <title>Login Admin</title>
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <div class="wrapper">
            <div class="header">
                <div class="logo">
                    <a href="index.php"><img src="../assets/images/logo4.png" width="200px"></a>
                </div>
            </div>
            <div class="panel">
                <ul>
                    <li><a href="list_berita.php"> List Berita</a></li>
                    <li><a href="list_kategori.php"> List Kategori</a></li>
                    <li><a href="list_admin.php"> List Admin</a></li>
                    <li><a href="page_kontak.php?id=1"> Page::Kontak</a></li>
                    <li><a href="page_tentang.php?id=2"> Page::Tentang</a></li>
                    <li><a href="logout.php"> Logout</a></li>
                </ul>
            </div>
            <div class="content">
